//
//  agentData.swift
//  valorant
//
//  Created by Christian Colman on 2022-11-03.
//

import Foundation


struct agent : Decodable { // Identifiable que es?
    let data : [data]
}

struct data : Decodable {
    let displayName : String
    let description : String
    let role : Role
}
struct Role : Decodable {
    let displayName : String
}
