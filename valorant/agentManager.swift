//
//  agentManager.swift
//  valorant
//
//  Created by Bootcamp 4 on 2022-11-03.
//

import Foundation

//Protocolo para visualizacion de datos
protocol agentManagerDelegate{
    func showAgentList(list: [agent])
}
//Variables generales
var agentList = [agent]()
var delegate:agentManagerDelegate?
struct agentManager{
//variables internas
    
    
    func seeAgents(){
        let string  = "https://valorant-api.com/v1/agents"
        if let url  = URL(string: string){
            let session = URLSession(configuration: .default)
            let task = session.dataTask(with: url, completionHandler: handler(data:response:error:))
            task.resume()
        }
    }
    
    func handler(data: Data?, response: URLResponse?, error: Error?){
        if  error != nil {
            print("The data request has been failed")
        }
        let info = parseJSON(data: data!)
        if info != nil {
            agentList = info!
            delegate?.showAgentList(list: agentList)
        }
        
    }
    
    func parseJSON(data: Data) -> [agent]?{
        let decoder = JSONDecoder()
        do{
            let clearInfo = try decoder.decode([agent].self, from: data)
            return clearInfo
        }catch{
            print("Error al decodificar los datos: ", error.localizedDescription)
            return nil
        }
    }
    
}
