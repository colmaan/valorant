//
//  ViewController.swift
//  valorant
//
//  Created by Christian Colman on 2022-11-03.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var agentTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        agentTable.delegate  = self
        agentTable.dataSource = self
    }


}

extension ViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celda = agentTable.dequeueReusableCell(withIdentifier: "celda", for: indexPath)
        celda.textLabel?.text = "CV"
        return celda
    }
    
    
}

